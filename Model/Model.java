package Model;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class Model {

    // FINALS
    public static final int WIDTH = 500;
    public static final int HEIGHT = 700;

    // Eigenschaften
    private Player player;

    private boolean startbildschirm = false;
    private boolean gameOver = false;
    private boolean gewonnen = false;
    private boolean spielGestartet = false;

    private List<Eisschollen> gewinnerSchollen = new LinkedList<>();
    private List<Eisschollen> verliererSchollen = new LinkedList<>();
    private List<Eisschollen> richtigerPfadSchollen = new LinkedList<>();

    private  final int[][] gewinnerPfad = {
            {100,450},
            {100,400}, {150,400}, {200,400},
                                  {200,350}, {250,350},
                                             {250,300},
                                             {250,250},
                                  {200,250},
                                  {200,200}
    };

    private  final int[][] spielfeld = {
            /*{100,450},*/ {150,450}, {200,450}, {250,450}, {300,450}, {350,450},
            /*{100,400},*/ /*{150,400},*/ /*{200,400},*/ {250,400}, {300,400}, {350,400},
            {100,350}, {150,350}, /*{200,350},*/ /*{250,350},*/ {300,350}, {350,350},
            {100,300}, {150,300}, {200,300}, /*{250,300},*/ {300,300}, {350,300},
            {100,250}, {150,250}, /*{200,250},*/ /*{250,250},*/ {300,250}, {350,250},
            {100,200}, {150,200}, /*{200,200},*/ {250,200}, {300,200}, {350,200},
    };


     /*private  final int[][] gewinnerPfad = {
            {200,450}, {200,400}, {250,400}, {250,350},
            {250,300}, {250,250}, {300,250}, {300,200}
    };*/
    /*private  final int[][] spielfeld = {
            {100,450}, {150,450}, *//*{200,450},*//* {250,450}, {300,450}, {350,450},
            {100,400}, {150,400}, *//*{200,400},*//* *//*{250,400},*//* {300,400}, {350,400},
            {100,350}, {150,350}, {200,350}, *//*{250,350},*//* {300,350}, {350,350},
            {100,300}, {150,300}, {200,300}, *//*{250,300},*//* {300,300}, {350,300},
            {100,250}, {150,250}, {200,250}, *//*{250,250},*//* *//*{300,250},*//* {350,250},
            {100,200}, {150,200}, {200,200}, {250,200}, *//*{300,200},*//* {350,200},
    };*/

    public Model() {

        for(int[] pos: spielfeld) {
            this.verliererSchollen.add(new Eisschollen(pos[0],pos[1]));
        }

        for (int[] pos: gewinnerPfad) {
            this.gewinnerSchollen.add(new Eisschollen(pos[0],pos[1]));
        }

        this.player = new Player(100, 450);
    }

    // Methoden

    public void checkCollision() {

        // Wenn der Spieler auf dem richtigen PFad ist: kennzeichne die Felder als korrekt
        Rectangle playerBounds = this.player.getBoundsOfPlayer();
        for (Eisschollen gewinnerScholle : this.gewinnerSchollen) {
            Rectangle gewinnerSchollenBounds = gewinnerScholle.getBoundsOfEisschollen();
            if (playerBounds.intersects(gewinnerSchollenBounds)) {
                richtigerPfadSchollen.add(gewinnerScholle);
            }
        }

        // Wenn der Spieler vom richtigen Pfad abkommt: Game Over!
        for (Eisschollen verliererScholle : this.verliererSchollen) {
            Rectangle verliererSchollenBounds = verliererScholle.getBoundsOfEisschollen();
            if (playerBounds.intersects(verliererSchollenBounds)) {
                this.gameOver = true;
            }
        }

        // Wenn der Spieler am Ziel: GEWONNNEEEEENNN!
        if (this.getPlayer().getY() == 150) {
            this.gewonnen = true;
        }
    }

    // Setter + Getter
    public List<Eisschollen> getGewinnerSchollen() { return gewinnerSchollen; }
    public List<Eisschollen> getRichtigerPfadSchollen() { return richtigerPfadSchollen; }

    public List<Eisschollen> getVerliererSchollen() { return verliererSchollen; }

    public Player getPlayer() { return this.player; }

    public void setStartbildschirm(boolean startbildschirm) { this.startbildschirm = startbildschirm; }
    public boolean isStartbildschirm() { return this.startbildschirm; }
    public boolean isGameOver() { return this.gameOver; }
    public boolean isGewonnen() { return this.gewonnen;}

    public boolean isSpielGestartet() { return spielGestartet; }

    public void setSpielGestartet(boolean spielGestartet) { this.spielGestartet = spielGestartet; }

}
