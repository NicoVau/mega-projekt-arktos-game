package Model;

import java.awt.*;

public class Eisschollen {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;

    // Konstruktor
    public Eisschollen(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 50;
        this.w = 50;
    }

    public int getX() { return x; }
    public int getY() { return y; }
    public int getH() { return h; }
    public int getW() { return w; }

    public Rectangle getBoundsOfEisschollen() {
        return new Rectangle(this.x,this.y,50,50);
    }

}
