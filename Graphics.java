import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import Model.*;


public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;

    Image startbildschirm = new Image("Bilder/Startbildschirm.png");
    Image runtimeBildschirm = new Image("Bilder/RuntimeBildschirm.png");
    Image loesungswegBildschirm = new Image("Bilder/LoesungswegBildschirm.png");
    Image gewinnerBildschirm = new Image("Bilder/GewinnerBildschirm.png");
    Image gameOverBildschirm = new Image("Bilder/GameOver.png");
    Image penguin = new Image("Bilder/penguin.png");
    Image floe = new Image("Bilder/floe.png");
    Image correctFloe = new Image("Bilder/correctFloe.png");

    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    // Methoden
    public void drawStartscreen() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Draw Startbildschirm
        gc.drawImage(startbildschirm,-436,0);
    }

    public void drawLoesungsweg() {

        //Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Draw LösungswegBildschirm
        gc.drawImage(loesungswegBildschirm,-436,0);

        // Draw Eisschollen
        for (Eisschollen feld : this.model.getVerliererSchollen()) {
            gc.drawImage(floe,feld.getX(),feld.getY());
        }
        for (Eisschollen feld : this.model.getGewinnerSchollen()) {
            gc.drawImage(correctFloe,feld.getX(),feld.getY());
        }
    }

    public void drawRuntime() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Draw RuntimeBildschirm
        gc.drawImage(runtimeBildschirm,-436,0);

        // Draw Eisschollen
        for (Eisschollen feld : this.model.getVerliererSchollen()) {
            gc.drawImage(floe,feld.getX(),feld.getY());
        }
        for (Eisschollen feld : this.model.getGewinnerSchollen()) {
            gc.drawImage(floe,feld.getX(),feld.getY());
        }
        for (Eisschollen feld : this.model.getRichtigerPfadSchollen()) {
            gc.drawImage(correctFloe,feld.getX(),feld.getY());
        }

        // Draw Player
        if (!model.isGameOver()) {
            gc.drawImage(penguin,
                    model.getPlayer().getX() - 10,
                    model.getPlayer().getY() - 70);
        }

    }

    public void drawGameOver() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Draw GameOverBildschirm
        gc.drawImage(gameOverBildschirm,-436,0);
    }

    public void drawGewonnen() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Draw GewinnerBildschirm
        gc.drawImage(gewinnerBildschirm,-436,0);
    }

}
