import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import javafx.stage.Stage;

import Model.Model;

public class Main extends Application {

    // Eigenschaften initialisieren
    private Timer timer;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        // Canvas
        Canvas meinCanvas = new Canvas(Model.WIDTH, Model.HEIGHT);

        // Group
        Group root = new Group();
        root.getChildren().add(meinCanvas);

        // Scene
        Scene meineScene = new Scene(root);

        // Stage
        primaryStage.setTitle("Moin!");
        primaryStage.setScene(meineScene);
        primaryStage.show();

        // Draw
        GraphicsContext gc = meinCanvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model, gc);
        timer = new Timer(model, graphics);
        timer.start();

        // InputHandler
        InputHandler inputHandler = new InputHandler(model);
        meineScene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );
    }
}