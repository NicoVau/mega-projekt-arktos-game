import javafx.animation.AnimationTimer;
import Model.Model;

public class Timer extends AnimationTimer {

    // Eigenschaften
    private long previousTime = -1;
    private Model model;
    private long totalTime = 0;

    private Graphics graphics;

    // Kontruktoren
    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long nowNano) {

        long nowMilli = nowNano/1000000;
        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMilli - previousTime;
        }
        previousTime = nowMilli;

        if (model.isStartbildschirm() == false) {
            graphics.drawStartscreen();

        } else {
            totalTime += elapsedTime;

            if (totalTime < 4000) {
                graphics.drawLoesungsweg();

            } else {
                model.setSpielGestartet(true);

                graphics.drawRuntime();
                model.checkCollision();

                if (model.isGameOver()) {
                    graphics.drawGameOver();

                } else if(model.isGewonnen()) {
                    graphics.drawGewonnen();
                }
            }
        }
    }
}
