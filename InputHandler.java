import javafx.scene.input.KeyCode;
import Model.Model;

public class InputHandler {

    // Eigenschaften
    private Model model;

    // Konstruktor
    public InputHandler(Model model) { this.model = model; }

    // Methoden


    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.ENTER) {
        model.setStartbildschirm(true);
        }
        else if (key == KeyCode.UP && model.isSpielGestartet()) {
            model.getPlayer().move(0,-50);
        }
        /*else if(key == KeyCode.DOWN && model.isSpielGestartet()) {
            model.getPlayer().move(0,50);
        }*/
        else if(key == KeyCode.LEFT && model.isSpielGestartet()) {
            model.getPlayer().move(-50,0);
        }
        else if(key == KeyCode.RIGHT && model.isSpielGestartet()) {
            model.getPlayer().move(50,0);
        }
    }
}
